<?php
/**
 * @file
 * Outputs the view.
 */

$base_path = base_path();
$library_path = libraries_get_path('supersized') . '/slideshow';

if ($options['views_jqfx_supersized']['ss_theme'] != 'none') :
?>

	<div id="views-jqfx-supersized-<?php print $id; ?>">

		<?php if ($options['views_jqfx_supersized']['navigation']['thumbnail_navigation'] != FALSE) : ?>
			<!--Thumbnail Navigation-->
			<div id="prevthumb"></div>
			<div id="nextthumb"></div>
		<?php endif; ?>

		
		<?php if ($options['views_jqfx_supersized']['navigation']['simple_navigation'] != FALSE) : ?>
			<!--Arrow Navigation-->
			<a id="prevslide" class="load-item"></a>
			<a id="nextslide" class="load-item"></a>
		<?php endif; ?>

		<?php if ($options['views_jqfx_supersized']['navigation']['thumbnail_navigation'] == '1') : ?>
			<div id="thumb-tray" class="load-item">
				<div id="thumb-back"></div>
				<div id="thumb-forward"></div>
			</div>
		<?php endif; ?>

		<!--Time Bar-->
		<?php if ($options['views_jqfx_supersized']['theme']['progress_bar'] == '1') : ?>
			<div id="progress-back" class="load-item">
				<div id="progress-bar"></div>
			</div>
		<?php endif; ?>

	<!--Control Bar-->
	<div id="controls-wrapper" class="load-item">
		<div id="controls">

			<a id="play-button"><img id="pauseplay" src="<?php print $library_path; ?>/img/pause.png"/></a>

			<!--Slide counter-->
			<div id="slidecounter">
				<span class="slidenumber"></span> / <span class="totalslides"></span>
			</div>

			<!--Slide captions displayed here-->
			<div id="slidecaption"></div>

			<?php if ($options['views_jqfx_supersized']['navigation']['thumb_links'] == '1') : ?>
				<!--Thumb Tray button-->
				<a id="tray-button"><img id="tray-arrow" src="<?php print $base_path; ?>sites/all/libraries/supersized/slideshow/img/button-tray-up.png"/></a>

				<!--Navigation-->
				<ul id="slide-list"></ul>
			<?php endif; ?>

		</div>
	</div>
</div>

<?php endif; ?>
